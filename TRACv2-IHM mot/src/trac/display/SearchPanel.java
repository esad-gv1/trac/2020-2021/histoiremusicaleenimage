package trac.display;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SearchPanel extends JPanel{
	private JTextField searchField = null;
	private JButton searchButton = null;
	
	public SearchPanel() {
		searchField = new JTextField(20);
		searchButton = new JButton("Rechercher");
		
		this.add(searchField);
		this.add(searchButton);
	}
}
