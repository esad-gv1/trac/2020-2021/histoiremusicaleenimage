package fr.esisar.trac.groupe1;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Panel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String adr_image;
	private JTextField textField;
	private JLabel l_image;
	private JLabel text;
	private ImageIcon image;
	
	public Panel() {
		super();
		this.set_prop_Panel();
	}
	
	private void set_prop_Panel() {
		this.setLayout(null);
		this.set_prop_text_entree();
		this.set_prop_img();
	}
	
	private void set_prop_text_entree() {
	textField = new JTextField();
	this.textField.setBounds(20, 20, 250, 20);
	this.add(textField);
	for(int i=0;i<5;i++) {
		set_prop_text(i);
	}
	}
	
	private void set_prop_img() {
		l_image = new JLabel();
		adr_image = "Images/babar.png";
		image = new ImageIcon(adr_image);
		this.l_image.setBounds(650, 400, 407, 482);
		this.l_image.setIcon(image);
		this.add(l_image);
	}
	
	private void set_prop_text(int i) {
		text = new JLabel();
		this.text.setBounds(20, 20*(i+2), 100, 20);
		this.text.setText("MOT"+Integer.toString(i));
		this.add(text);
	}
}
