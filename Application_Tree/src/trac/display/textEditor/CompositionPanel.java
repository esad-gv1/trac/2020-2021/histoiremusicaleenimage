package trac.display.textEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import trac.text.Word;

/*
 *Cette classe construit tout le panel de composition (editeur de texte, label "phrase en composition", boutons) 
 */
public class CompositionPanel extends JPanel {

	private Set<Word> words = null;
	private EditorPanel editorPanel;
	private SentencePanel sentencePanel;
	private ButtonPanel buttonPanel;
	private TreePanel treePanel;

	public CompositionPanel(Set<Word> words,Point origin,Dimension size) {
		super();
		this.words = words;
		
		this.setSize(size);
		this.setLocation(origin);
		
		// initialisation des composants graphiques
		editorPanel = new EditorPanel();		
		sentencePanel = new SentencePanel();
		treePanel = new TreePanel("ressources");
		buttonPanel = new ButtonPanel();
		
		//this.setLayout(new BorderLayout());
		
		this.add(sentencePanel);
		this.add(editorPanel);
		this.add(buttonPanel);
		this.add(treePanel);
	}
	
	public ButtonPanel getButtonPanel() {
		return buttonPanel;
	}

	public void setButtonPanel(ButtonPanel buttonPanel) {
		this.buttonPanel = buttonPanel;
	}

	public EditorPanel getEditorPanel() {
		return editorPanel;
	}

	public SentencePanel getSentencePanel() {
		return sentencePanel;
	}	
}
