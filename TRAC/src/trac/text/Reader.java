package trac.text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;

import trac.audio.Player;

public class Reader {
	private FileInputStream fis = null;
	private Map <String,String> triggerMap;
	private String filePath;
	
	
	
	public Reader(Map<String, String> triggerMap, String filePath) {
		super();
		this.triggerMap = triggerMap;
		this.filePath = filePath;
	}

	public void start() {
		//ouverture fichier
		try {
			fis = new FileInputStream(new File(filePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//lecture fichier mot par mot
		Word word;
		Player player = null;
		try {
			word = getWord();
			while(!word.isEndOfFile()) {
				System.out.print(word.getWord());
				//System.out.println("[DEBUG]"+word.getTrigger()+"|");
				if(word.getTrigger() != null) {
					System.out.print("[trigger]");
					player = new Player(word.getTrigger());
					player.start();
				}
				
				Thread.sleep(200);
				
				word = getWord();
			}
			System.out.println("Fin du texte");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public Word getWord() throws IOException {
		int car;
		String strWord = "";
		
		boolean carriageReturn = false;
		boolean endOfFile = false;
		String trigger = null;
		
		
		do {
			car = fis.read();
			if(car == -1) {
				break;
			}
			strWord += (char)car;
			
		}while((byte)car != ' ' && (byte)car != '\n');
		
		//System.out.println("[DEBUG]"+strWord+"|");
		if(triggerMap.containsKey(strWord)) {
			trigger = triggerMap.get(strWord);
		}
		
		if((byte)car == '\n') {
			carriageReturn = true;
		}
		
		if(car == -1) {
			endOfFile = true;
		}
		
		
		
//		strWord = Normalizer.normalize(strWord, Normalizer.Form.NFD);
//		strWord = strWord.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		
		return(new Word(strWord, trigger, carriageReturn, endOfFile));	
	}
}
